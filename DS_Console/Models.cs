﻿using Newtonsoft.Json;
using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Snippet;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DS_Console
{
    class MyTestClass
    {
        const string ConfigFileName = "myConfig.json";
        public List<TestService> Services { get; set; }

        public MyTestClass()
        {
            Services = new List<TestService>();
        }

        public string GetPath()
        {
            return Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\";
        }

        public MyTestClass CreateSampleData()
        {
            var myTestClass = new MyTestClass { };
            var myService1 = new TestService { ThreadNumbers = 10, ApiKey = " Your Api Key ", ServiceName = "Store-Products", WriteToFile = false, ServiceUrl = "http://www.google.com", Enabled = false, DelaysBetweenThreads = 0};
            var myService2 = new TestService { ThreadNumbers = 10, ApiKey = " Your Api Key ", ServiceName = "Stores", WriteToFile = true, ServiceUrl = "https://api.dansksupermarked.dk/v1/stores", Enabled = true , DelaysBetweenThreads = 0};
            myTestClass.Services.Add(myService1);
            myTestClass.Services.Add(myService2);
            myTestClass.CreateConfigFile();
            return myTestClass;
        }

        public void CreateConfigFile()
        {
            using (FileStream fs = File.Open(GetPath() + ConfigFileName, FileMode.CreateNew))
            using (StreamWriter sw = new StreamWriter(fs))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jw, this);
            }
        }

        public MyTestClass LoadConfig()
        {
            try
            {
                string myJsonConfigFile = Snippet.Snippet.ReadFile(GetPath() + ConfigFileName);
                string.Format("reading config from {0}", GetPath() + ConfigFileName).ToConsoleWithTime();
                return JsonConvert.DeserializeObject<MyTestClass>(myJsonConfigFile);
            }
            catch (FileNotFoundException e)
            {
                Console.Write(string.Format("Could not find config file at {0} -> create a fresh one",
                    GetPath() + ConfigFileName));
                MyTestClass myTestClass = CreateSampleData();
                CreateConfigFile();
                return myTestClass;
            }
            catch (Exception e)
            {
                throw new Exception("Failure Loading config " + e.InnerException);
            }
        }
    }

    public class TestService
    {
        public string ServiceName { get; set; }
        public bool Enabled { get; set; }
        public string ApiKey { get; set; }
        public int ThreadNumbers { get; set; }
        public string ServiceUrl { get; set; }
        public bool WriteToFile { get; set; }
        public int DelaysBetweenThreads { get; set; }

        public override string ToString()
        {
            return string.Format("Name={0} Url={1} delay={2} ToFile={3}", ServiceName, ServiceUrl, DelaysBetweenThreads,WriteToFile);
        }

    }

    public class MyHttpResults
    {
        public int Count { get; set; }
        public string StatusCode { get; set; }
        public string HtmlBody { get; set; }
        public HttpWebResponse ServerHttpWebResponse { get; set; }
        public string Exception { get; set; }

        public override string ToString()
        {
                return string.Format("count={0} code={1} len={2}", Count, StatusCode, HtmlBody.Length);
             
        }
    }

}
