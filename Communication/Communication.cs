﻿using Newtonsoft.Json.Linq;
using Snippet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationKlingstenNet
{

    public interface ICommunication
    {
        Task<string> GetApiResponse(string url);
  
    }

    public class Communication : ICommunication
    {
        public async Task<string> GetApiResponse(string url)
        {
            string discoverContent;
            try
            {
                using (var client = new HttpClient())
                {
                    using (var apiRequest = new HttpRequestMessage(HttpMethod.Get, url))
                    {


                        apiRequest.Headers.Add("Authorization", "Bearer {token:https://api.office.com/discovery/}");
                        using (var reponse = await client.SendAsync(apiRequest))
                        {
                            reponse.IsSuccessStatusCode.ToString().ToConsoleWithTime();
                            discoverContent = await reponse.Content.ReadAsStringAsync();
                        }
                    }
                }
            }
            catch (WebException e)
            {
                discoverContent = e.Message;
            }
            catch (Exception ex)
            {
                discoverContent = ex.Message.ToString();
            }
            return discoverContent;
        }
    }



}


