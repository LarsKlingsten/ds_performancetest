﻿using System;
using Snippet;
using DS_Console;

namespace DS_Console {
    class RunThis {

        private string testSite = "http://google.com";
        public static void Main(String[] args) {

            var runThis = new RunThis();
            //  runThis.TestWebRequester();
            //  runThis.TestWebClientRunMulti();

            //   runThis.TestWebRequester();

            runThis.TestWebP();

            Console.ReadLine();
        }

        public void TestWebP() {
            var webRequest = new MultiThreadWebPerformance();
            webRequest.RunTest();

        }


        public void TestWebRequester() {
            var webRequest = new MultiThreadWebPerformance();
            webRequest.HttpClientGetAsync(testSite);
            webRequest.HttpClientDownloadString(testSite);
            webRequest.mydownloadAsyncReUse();
        }


        public void TestWebClientRunMulti() {
            "TestWebClient2".ToConsoleWithTime();
            for (int i = 0; i < 6; i++) {
                 TestWebClient2();
                System.Threading.Thread.Sleep(300);
            }

        }

        public async void TestWebClient2() {
            var timer = new MyTimer();
            ICancellationTokenSourceFactory cancellationTokenSourceFactory = new SystemCancellationTokenSourceFactory();
             IApiClient vk = new ApiClient();
             ApiRequest apiRequest = new  ApiRequest {
                Url = testSite
            };
             ApiResponse apiResponse = await vk.GetApiResponse(apiRequest);
            string.Format("{0} {1} mills", apiResponse, timer.ElapsedMilliSec()).ToConsoleWithTime();

        }
    }
}
