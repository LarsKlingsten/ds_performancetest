﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.Net.Http;
using Snippet;

namespace DS_Console {

    #region factories

    public interface ICancellationTokenSourceFactory {
        ICancellationTokenSource Create();
    }

    public interface ICancellationTokenSource : IDisposable {
        CancellationToken Token { get; }
        void CancelAfter(int millisecondsDelay);
    }

    public class SystemCancellationTokenSourceFactory : ICancellationTokenSourceFactory {
        public ICancellationTokenSource Create() {
            return new SystemCancellationTokenSource();
        }
    }

    public class SystemCancellationTokenSource : CancellationTokenSource, ICancellationTokenSource { }

    public interface IWebClient : IDisposable {
        Task<string> DownloadStringTaskAsync(string address);
        void CancelAsync();
        WebHeaderCollection ResponseHeaders { get; }
        void SetHeader(HttpRequestHeader header, string value);
        void SetEncoding(Encoding encoding);
    }

    public interface IWebClientFactory {
        IWebClient Create();
    }

    public class SystemWebClientFactory : IWebClientFactory {
        public IWebClient Create() {
            return new SystemWebClient();
        }
    }

    public class SystemWebClient : WebClient, IWebClient {
        public void SetHeader(HttpRequestHeader header, string value) {
            this.Headers.Add(header, value);
        }

        public void SetEncoding(Encoding encoding) {
            this.Encoding = encoding;
        }
    }

    #endregion

    public interface IApiClient {
        Task<ApiResponse> GetApiResponse(ApiRequest apiRequest);
    }
    
    class ApiClient : IApiClient {

        public async Task<ApiResponse> GetApiResponse(ApiRequest apiRequest) {
            var apiResponse = new ApiResponse();
            try {
                using (var webClient = new SystemWebClient())
                using (ICancellationTokenSource cts = new SystemCancellationTokenSourceFactory().Create()) {
                 //   var url = string.Format(apiRequest.Url);
                    var ct = cts.Token;
                    ct.Register(webClient.CancelAsync);
                    cts.CancelAfter(apiRequest.Timeout);

                    try {
                        apiResponse.Body = await webClient.DownloadStringTaskAsync(apiRequest.Url);
                        apiResponse.HttpCode = (int) HttpStatusCode.OK;
                        apiResponse.HttpMessage = HttpStatusCode.OK.ToString();
                        apiResponse.Headers = webClient.Headers;
                        return apiResponse;

                    } catch (WebException we) {
                        apiResponse.Body = we.InnerException.ToString();
                        apiResponse.HttpMessage = HttpStatusCode.RequestTimeout.ToString() + " Request timed out";
                        apiResponse.HttpCode = (int) HttpStatusCode.RequestTimeout  ;
                        return apiResponse;
                    }
                }
            } catch (WebException ex) {
                var webResponse = (HttpWebResponse)  ex.Response ;
                apiResponse.HttpCode = webResponse != null ? (int) webResponse.StatusCode : (int)HttpStatusCode.InternalServerError;
                apiResponse.Headers = webResponse != null ? webResponse.Headers : new WebHeaderCollection();
                apiResponse.HttpMessage = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                return apiResponse;
            } catch (Exception ex) {
                apiResponse.HttpCode = (int)HttpStatusCode.InternalServerError;
                apiResponse.Headers = new WebHeaderCollection();
                apiResponse.HttpMessage = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                return apiResponse;
            }
        }

    }
    public class ApiResponse {
        public string Body { get; set; }
        public string HttpMessage { get; set; }
        public int HttpCode { get; set; }
        public WebHeaderCollection Headers { get; set; }

        override
        public string ToString() {
            return string.Format(" body={0} message={1} code={2}", Body.Left(20), HttpMessage.Left(10), HttpCode);
        }

    }

    public class ApiRequest {
        public string Url;
        public Encoding Encoding;
        public Header Headers;
        public HttpMethod RequestMethod;
        public string Payload;
        public string MediaType;
        public int Timeout;

        public ApiRequest() {
            Encoding = Encoding.UTF8;
            MediaType = "application/json";
            Timeout = 500;

        }

        public class Header {
            public string Key { get; set; }
            public string Value { get; set; }
        }
    }










}
