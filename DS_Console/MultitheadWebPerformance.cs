﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Snippet;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DS_Console {
    class MultiThreadWebPerformance {

        private static MyTimer _timer = new MyTimer();
        private Dictionary<String, MyHttpResults> _resultDictionary;

        public void RunTest() {

            MyTestClass myTestClass = new MyTestClass().LoadConfig();
            MultiThreadWebPerformance performance = new MultiThreadWebPerformance();

            foreach (TestService service in myTestClass.Services) {
                if (service.Enabled) {
                    string.Format("Testing ={0} Threads={1} delays={2}", service.ServiceName, service.ThreadNumbers, service.DelaysBetweenThreads).ToConsoleWithTime();
                    service.ServiceUrl.ToConsoleWithTime();

                    var timer = new MyTimer();
                    performance._resultDictionary = new Dictionary<string, MyHttpResults>();
                    performance.MyThreadPool(service);
                    performance.WriteDictionaryConclusion(myTestClass, service);
                    string.Format("Completed in {0}", timer.ElapsedMilliSec()).ToConsoleWithTime();
                    "---".ToConsoleWithTime();
                }
            }
        }

        #region WriteConclusion
        private void WriteDictionaryConclusion(MyTestClass testClass, TestService testService) {
            foreach (var key in _resultDictionary.Keys) {
                MyHttpResults myHttpResults;
                _resultDictionary.TryGetValue(key, out myHttpResults);

                if (myHttpResults != null) {
                    myHttpResults.ToString().ToConsoleWithTime();

                    if (testService.WriteToFile) {
                        try {
                            StreamWriter streamWriter =
                                new StreamWriter(testClass.GetPath() + "result_" + testService.ServiceName + ".txt");
                            string.Format("Results saved here {0}",
                                testClass.GetPath() + "result_" + testService.ServiceName + ".txt").ToConsoleWithTime();
                            streamWriter.WriteLine(DateTime.UtcNow + " " + testService.ServiceName);
                            streamWriter.WriteLine(testService.ToString());
                            streamWriter.WriteLine(myHttpResults.ToString());
                            streamWriter.WriteLine("");
                            streamWriter.WriteLine(myHttpResults.HtmlBody);
                            streamWriter.WriteLine("");
                            streamWriter.WriteLine("---");
                            streamWriter.WriteLine("");
                            streamWriter.Close();
                        } catch (Exception e) {
                            throw new Exception("WriteDictionaryConclusion:SteamWriter:" + e.Message);
                        }

                    }
                }
            }
        }

        #endregion

        public void MyThreadPool(TestService testTestService) {
            int MAXTHREADS = testTestService.ThreadNumbers;
            ServicePointManager.DefaultConnectionLimit = MAXTHREADS;
            ThreadPool.SetMaxThreads(MAXTHREADS, MAXTHREADS);

            var threads = new List<Thread>(MAXTHREADS);
            for (int p = 0; p < MAXTHREADS; p++) {
                Thread myThread = new Thread(() => GetResponsesFromService(testTestService));
                myThread.Priority = ThreadPriority.AboveNormal;
                threads.Add(myThread);
                myThread.Start();

                Thread.Sleep(testTestService.DelaysBetweenThreads);

            }
            foreach (var thread in threads) {
                thread.Join();             // Wait for completion of all threads
            }
        }

        public void GetResponsesFromService(TestService testTestService) {

            var timer = new MyTimer();

            MyHttpResults apiResponse = GetApiResponse(testTestService);
            MyHttpResults exitingResponse;
            if (apiResponse.Exception != null) {
                apiResponse.StatusCode = apiResponse.Exception.RemoveNonDigits();
                apiResponse.HtmlBody = apiResponse.Exception;
            } else {
                apiResponse.StatusCode = apiResponse.ServerHttpWebResponse.StatusCode + "";
            }
            string myKey = apiResponse.StatusCode + " " + apiResponse.HtmlBody.Length;

            string.Format("StatusCode={0} len={1}  elapsed={2} millisec", apiResponse.StatusCode, apiResponse.HtmlBody.Length, timer.ElapsedMilliSec()).ToConsoleWithTime();
         
            _resultDictionary.TryGetValue(myKey, out exitingResponse);
            if (exitingResponse == null) {
                apiResponse.Count = 1;
                _resultDictionary.Add(myKey, apiResponse);
            } else {
                exitingResponse.Count++;
            }
        }

        public MyHttpResults GetApiResponse(TestService testService) {
            Uri myUri = new Uri(testService.ServiceUrl);
            WebRequest myWebRequest = HttpWebRequest.Create(myUri);
            myWebRequest.Proxy = null;
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)myWebRequest;
            NetworkCredential myNetworkCredential = new NetworkCredential(testService.ApiKey, "");
            CredentialCache myCredentialCache = new CredentialCache();
            myCredentialCache.Add(myUri, "Basic", myNetworkCredential);
            myHttpWebRequest.PreAuthenticate = true;
            myHttpWebRequest.Credentials = myCredentialCache;

            try {
                WebResponse myWebResponse = myWebRequest.GetResponse();
                HttpWebResponse httpWebResponse = (HttpWebResponse)myWebResponse;
                Stream responseStream = myWebResponse.GetResponseStream();
                StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8);
                string htmlBody = streamReader.ReadToEnd();
                responseStream.Close();
                myWebResponse.Close();
                return new MyHttpResults { ServerHttpWebResponse = httpWebResponse, HtmlBody = htmlBody };
            } catch (WebException we) {
                return new MyHttpResults { ServerHttpWebResponse = null, HtmlBody = we.ToString(), Exception = we.Message };
            }
        }

        public void HttpClientDownloadString(string address) {
            var sw = new MyTimer();

            WebClient client = new WebClient();
            try {
                string response = client.DownloadString(address);

                string.Format("len={0} sec={1}", response.Length, sw.ElapsedString()).ToConsoleWithTime();
            } catch (WebException we) {
                we.Message.ToString().ToConsoleWithTime();
            }

            string.Format("completed in {0} millisecs", sw.ElapsedMilliSec()).ToConsoleWithTime();

        }


        public async void HttpClientGetAsync(string address) {

            var sw = new MyTimer();

            using (var client = new HttpClient()) {

              
                // New code:
                HttpResponseMessage response = await client.GetAsync(address);
                string result = await response.Content.ReadAsStringAsync();

                string.Format("GetAsync len={0} sec={1}", result.Length, sw.ElapsedString()).ToConsoleWithTime();
            }

        }


        public static void mydownloadAsync() {

            var timer = new MyTimer();
            WebClient cli = new WebClient();
            cli.DownloadStringCompleted += (sender, e) => Console.WriteLine((e.Result.Length + " mytimer " + timer.ElapsedMilliSec()));
            cli.DownloadStringAsync(new Uri("http://195.249.20.244")); // Blocks until request has been prepared

            for (int i = 0; i < 15; i++) {
                Console.WriteLine(i);
                Thread.Sleep(100);
            }
        }

        public void mydownloadAsyncReUse() {

            var timer = new MyTimer();
            WebClient cli = getWebClient();
            cli.DownloadStringCompleted += (sender, e) => Console.WriteLine((e.Result.Length + " (reused) mytimer " + timer.ElapsedMilliSec()));
            cli.DownloadStringAsync(new Uri("http://195.249.20.244")); // Blocks until request has been prepared

           
        }

        public void mydownloadAsync2() {

            var timer = new MyTimer();
            WebClient cli = new WebClient();
            cli.Proxy = null;

            string mystr = "nothing";
            cli.DownloadStringCompleted += (sender, e) => printMe(mystr = " call method -> len" + e.Result.Length + " timer=" + timer.ElapsedMilliSec());
            cli.DownloadStringAsync(new Uri("http://www.google.com")); // Blocks until request has been prepared


            Console.WriteLine(mystr);
        }

        public static void printMe(string str) {
            Console.WriteLine(str);
        }


        private static WebClient _webClient = new WebClient();

        public static WebClient getWebClient() {
            return _webClient;
        }
    }
}
