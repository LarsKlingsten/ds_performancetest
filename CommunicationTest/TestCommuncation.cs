﻿using CommunicationKlingstenNet;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace CommunicationTest
{
    [TestFixture]
    public class TestCommuncation
    {
        private NewsLetters letters;
        private Mock<ICommunication> mockCommuncation;

        [SetUp]
        public void Init()
        {
          
        }

        [Test]
        public async void TestMock()
        {
            //arrange
            mockCommuncation = new Mock<ICommunication>();
            mockCommuncation
                .Setup(c => c.GetApiResponse(It.IsAny<string>()))
                .Returns(Task.FromResult("SomeMockResponse"));

            letters = new NewsLetters(mockCommuncation.Object);

            //assess
            var result = await letters.TestRun();


            //assert
            Assert.AreEqual("SomeMockResponse", result);



        }
    }
}
