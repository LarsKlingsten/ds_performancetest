﻿using Snippet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationKlingstenNet
{
    public class NewsLetters
    {
        private readonly ICommunication communication;

        public NewsLetters()
        {
            communication = new Communication();
        }
        public NewsLetters(ICommunication communication)
        {
            this.communication = communication;
        }

        public async Task<string> TestRun()
        {
            string myString = await communication.GetApiResponse(" https://www.google.dk");
            myString.ToConsoleWithTime();
            return myString;
        }

    }
}
