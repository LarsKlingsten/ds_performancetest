﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Snippet;
using System.Net;
using System.Threading;
using System.IO;
using System.Runtime.Remoting.Messaging;

namespace DS_Console {
    class OldStuff {

        private static readonly string DS_API_URI = "https://api.dansksupermarked.dk/v1/";
        private static readonly string DS_KEY = "d1a73fee-707e-40fd-b2fe-4d9710f891ab";

        public delegate string AsyncMethodCaller(string command, out int threadId); // same signature a the sync method
        private static MyTimer _timer = new MyTimer();


        private void StressTestReadAsync() {
            MyTimer timePart = new MyTimer();
            MyTimer timeTotal = new MyTimer();
            "StressTestReadAsync".ToConsoleWithTime();
            for (int i = 0; i < 10; i++) {
                timePart.Reset();
                TestAsyncWithCallback();
            }
            string.Format("Completed StressTestReadAsync() in {0} secs",timeTotal.ElapsedMilliSec()).ToConsoleWithTime();
        }

        private void TestAsyncWithCallback() {
            AsyncMethodCaller caller = new AsyncMethodCaller(ReadDSApiAsync);

            int dummy = 0; // value is not used (but is required)
            IAsyncResult result = caller.BeginInvoke("stores",
                            out dummy,
                            new AsyncCallback(CallbackMethod),
                            "The call executed on thread {0}, with return value \"{1}\".");
            string.Format("The main thread {0} continues to execute... ", Thread.CurrentThread.ManagedThreadId).ToConsoleWithTime();
        }

        // The callback method must have the same signature as the 
        // AsyncCallback delegate. 
        static void CallbackMethod(IAsyncResult ar) {
            // Retrieve the delegate.
            AsyncResult result = (AsyncResult)ar;
            AsyncMethodCaller caller = (AsyncMethodCaller)result.AsyncDelegate;

            string formatString = (string)ar.AsyncState;

            int threadId = 0;

            // Call EndInvoke to retrieve the results. 
            string returnValue = caller.EndInvoke(out threadId, ar);

           // string.Format("{_timer.ElapsedMilliSec()} sec callback: The thread { threadId} returned {returnValue.Length}".ToConsoleWithTime());
        }


        private string ReadDSApiAsync(string command, out int threadId) {
            threadId = Thread.CurrentThread.ManagedThreadId;
            Uri myUri = new Uri(DS_API_URI + command);
            WebRequest myWebRequest = WebRequest.Create(myUri);
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)myWebRequest;
            NetworkCredential myNetworkCredential = new NetworkCredential(DS_KEY, "");
            CredentialCache myCredentialCache = new CredentialCache();
            myCredentialCache.Add(myUri, "Basic", myNetworkCredential);
            myHttpWebRequest.PreAuthenticate = true;
            myHttpWebRequest.Credentials = myCredentialCache;

            WebResponse myWebResponse;
            try {
                myWebResponse = myWebRequest.GetResponse();
            } catch (Exception e) {
                return e.ToString();
            }
            Stream responseStream = myWebResponse.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(responseStream, Encoding.UTF8);
            string result = myStreamReader.ReadToEnd();
            responseStream.Close();
            myWebResponse.Close();
            return result;
        }


        private void TestAsync() {
            int threadId;
            AsyncMethodCaller caller = new AsyncMethodCaller(ReadDSApiAsync);

            IAsyncResult result = caller.BeginInvoke("stores", out threadId, null, null);

            Thread.Sleep(0);
        //    $"mainThread does work #{Thread.CurrentThread.ManagedThreadId}".ToConsoleWithTime();
            string returnValue = caller.EndInvoke(out threadId, result);
         //   $"The call executed on thread {threadId}, with return value {returnValue.Length}".ToConsoleWithTime();
        }

    }
    
}